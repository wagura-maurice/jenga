@extends('admin.home')
@section('main_content')
<div id='content' class='content'>
            <ol class="breadcrumb hidden-print pull-right">
                <li><a href="javascript:;">Home</a></li>
                <li class="active">Groups</li>
            </ol>
            <h1 class="page-header hidden-print">Groups <small>groups report goes here...</small></h1>
            <div class="row no-print">
                <div class="col-md-12 hidden-print">
                    <a href="{!! route('groups.index') !!}"><button type="button" class="btn btn-inverse btn-icon btn-circle m-b-10"><i class="fa fa-arrow-left"></i></button></a>
                </div>
            </div>
            <div class="invoice">
                <div class="invoice-company">
                    <span class="pull-right hidden-print">
                    <a href="javascript:;" class="btn btn-sm btn-success m-b-10"><i class="fa fa-download m-r-5"></i> Export as PDF</a>
                    <a href="javascript:;" onclick="window.print()" class="btn btn-sm btn-success m-b-10"><i class="fa fa-print m-r-5"></i> Print</a>
                    </span>
                </div>
                <div class="invoice-header">
                    <div class="invoice-from">
                        <address class="m-t-5 m-b-5">
                            <strong>{!! $groupsdata['company'][0]->name!!}</strong><br/>
                            {!! $groupsdata['company'][0]->street!!} {!! $groupsdata['company'][0]->address!!}<br />
                            {!! $groupsdata['company'][0]->city!!}, {!! $groupsdata['company'][0]->zip_code!!}<br />
                            Phone: {!! $groupsdata['company'][0]->phone_number!!}<br />
                        </address>
                    </div>
                    <div class="invoice-date">
                        <img src="{!!asset('uploads/images/'.$groupsdata['company'][0]->logo)!!}" width='100' height='100'/>
                    </div>
                </div>
                <div class="invoice-content">
                    <div class="table-responsive">
                        <table class="table table-invoice">
                        <thead>
                                    <tr>
                                        <th>Group Number</th>
                                        <th>Group Name</th>
                                        <th>Meeting Day</th>
                                        <th>Meeting Time</th>
                                        <th>Meeting Frequency</th>
                                        <th>Preferred Mode Of Banking</th>
                                        <th>Registration Date</th>
                                        <th>Group Formation Date</th>                                    </tr>
                        </thead>
                        <tbody>
                        @foreach ($groupsdata['list'] as $groups)
                            <tr>
                                <td class='table-text'><div>
                                    {!! $groups->group_number !!}
                                </div></td>
                                <td class='table-text'><div>
                                    {!! $groups->group_name !!}
                                </div></td>
                                <td class='table-text'><div>
                                    {!! $groups->meetingdaymodel->name !!}
                                </div></td>
                                <td class='table-text'><div>
                                    {!! $groups->meeting_time !!}
                                </div></td>
                                <td class='table-text'><div>
                                    {!! $groups->meetingfrequencymodel->name !!}
                                </div></td>
                                <td class='table-text'><div>
                                    {!! $groups->preferredmodeofbankingmodel->name !!}
                                </div></td>
                                <td class='table-text'><div>
                                    {!! $groups->registration_date !!}
                                </div></td>
                                <td class='table-text'><div>
                                    {!! $groups->group_formation_date !!}
                                </div></td>
                            </tr>
                        @endforeach
                        </tbody>
                        </table>
                    </div>
                    <div class="invoice-price">
                        <div class="invoice-price-left">
                            <div class="invoice-price-row">
                            </div>
                        </div>
                        <div class="invoice-price-right">
                            <small>TOTAL COUNT</small> {!!count($groupsdata['list'])!!}
                        </div>
                    </div>
                </div>
                <div class="invoice-footer text-muted">
                    <p class="text-center m-b-5">
                    </p>
                    <p class="text-center">
                        <span class="m-r-10"><i class="fa fa-globe"></i> {!! $groupsdata['company'][0]->website!!}</span>
                        <span class="m-r-10"><i class="fa fa-phone"></i> T:{!! $groupsdata['company'][0]->phone_number!!}</span>
                        <span class="m-r-10"><i class="fa fa-envelope"></i> {!! $groupsdata['company'][0]->email_address!!}</span>
                    </p>
                </div>
            </div>
</div>
@endsection