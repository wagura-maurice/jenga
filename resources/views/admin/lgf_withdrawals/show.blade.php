@extends('admin.home')
@section('main_content')
		<div id="content" class="content">
    <ol class="breadcrumb pull-right">
        <li><a href="#">Home</a></li>
        <li><a href="#">public</a></li>
        <li><a href="#">Lgf Withdrawals</a></li>
        <li class="active">form</li>
    </ol>
    <h1 class="page-header">Lgf Withdrawals Form <small>lgf withdrawals details goes here...</small></h1>
    <div class="row">
        <div class="col-md-12">
            <a href="{!! route('lgfwithdrawals.index') !!}"><button type="button" class="btn btn-inverse btn-icon btn-circle m-b-10"><i class="fa fa-arrow-left"></i></button></a>
        </div>
    </div>
            <div class="profile-container">
                <div class="profile-section">
                        <div class="profile-info">
                            <div class="table-responsive">
                                <table class="table table-profile">
                                    <thead>
                                    	<tr class="highlight">
                                    		<td>Lgf Withdrawals</td>
                                    		<td>Profile Data</td>
                                    	</tr>
                                        <tr class="divider">
                                            <td colspan="2"></td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="field">Client</td>
                                            <td>				                                @foreach ($lgfwithdrawalsdata['clients'] as $clients)
				                                @if( $clients->id  ==  $lgfwithdrawalsdata['data']->client  )
				                                {!! $clients->client_number!!}
				                                {!! $clients->first_name!!}
				                                {!! $clients->middle_name!!}
				                                {!! $clients->last_name!!}
				                                </option>@endif
				                                @endforeach
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="field">Mode Of Withdrawal</td>
                                            <td>				                                @foreach ($lgfwithdrawalsdata['lgfwithdrawalmodes'] as $lgfwithdrawalmodes)
				                                @if( $lgfwithdrawalmodes->id  ==  $lgfwithdrawalsdata['data']->mode_of_withdrawal  )
				                                {!! $lgfwithdrawalmodes->name!!}
				                                </option>@endif
				                                @endforeach
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="field">Amount</td>
                                            <td>
                                            {!! $lgfwithdrawalsdata['data']->amount !!}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="field">Date</td>
                                            <td>
                                            {!! $lgfwithdrawalsdata['data']->date !!}
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                </div>
            </div>
		</div>