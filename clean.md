/usr/bin/php7.4 artisan down
/usr/bin/php7.4 artisan cache:clear
/usr/bin/php7.4 artisan route:clear
/usr/bin/php7.4 artisan config:clear
/usr/bin/php7.4 artisan view:clear
/usr/bin/php7.4 artisan optimize
/usr/bin/php7.4 artisan up
/usr/local/bin/composer dump-autoload
