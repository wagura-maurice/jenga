<?php

namespace App\Providers;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Illuminate\Pagination\LengthAwarePaginator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if (env('APP_ENV') == 'production') {
            URL::forceScheme('https');
        }

        Schema::defaultStringLength(191);

        /**
         * Paginate a standard Laravel Collection.
         *
         * @param int $perPage
         * @param int $total
         * @param int $page
         * @param string $pageName
         * @return array
         */
        Collection::macro('paginate', function($perPage, $total = null, $page = null, $pageName = 'page') {
            $page = $page ?: LengthAwarePaginator::resolveCurrentPage($pageName);

            return new LengthAwarePaginator(
                $this->forPage($page, $perPage),
                $total ?: $this->count(),
                $perPage,
                $page,
                [
                    'path' => LengthAwarePaginator::resolveCurrentPath(),
                    'pageName' => $pageName,
                ]
            );
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        require_once app_path() . '/Helpers/JENGA.php';

        // DB::statement('SET SESSION sql_require_primary_key=0');

        /*
        |-----------------------------------------------
        | Load domain-specific .env file if it exists
        |-----------------------------------------------
        */

        if(isset($_SERVER['HTTP_HOST']) && !empty($_SERVER['HTTP_HOST']) ) {
            $data = explode('.', $_SERVER['HTTP_HOST']);
            config([
                'app.name' => strtoupper('epiphium technologies - ' . $data[0]),
                'app.url' => 'http://' . $data[0] . '.' . $data[1] . '.com',
                'database.connections.mysql.database' => $data[0]
            ]);
        }
    }
}
