<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\MpesaTransactionFees;
use Illuminate\Http\Request;

class MpesaTransactionFeesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\MpesaTransactionFees  $mpesaTransactionFees
     * @return \Illuminate\Http\Response
     */
    public function show(MpesaTransactionFees $mpesaTransactionFees)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\MpesaTransactionFees  $mpesaTransactionFees
     * @return \Illuminate\Http\Response
     */
    public function edit(MpesaTransactionFees $mpesaTransactionFees)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\MpesaTransactionFees  $mpesaTransactionFees
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MpesaTransactionFees $mpesaTransactionFees)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\MpesaTransactionFees  $mpesaTransactionFees
     * @return \Illuminate\Http\Response
     */
    public function destroy(MpesaTransactionFees $mpesaTransactionFees)
    {
        //
    }
}
