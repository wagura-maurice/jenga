<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\FinePaymentFees;
use Illuminate\Http\Request;

class FinePaymentFeesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\FinePaymentFees  $finePaymentFees
     * @return \Illuminate\Http\Response
     */
    public function show(FinePaymentFees $finePaymentFees)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\FinePaymentFees  $finePaymentFees
     * @return \Illuminate\Http\Response
     */
    public function edit(FinePaymentFees $finePaymentFees)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\FinePaymentFees  $finePaymentFees
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FinePaymentFees $finePaymentFees)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\FinePaymentFees  $finePaymentFees
     * @return \Illuminate\Http\Response
     */
    public function destroy(FinePaymentFees $finePaymentFees)
    {
        //
    }
}
